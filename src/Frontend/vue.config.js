const webpack = require('webpack')
const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
    transpileDependencies: true,
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                Buffer: ['buffer', 'Buffer']
            })
        ],
        resolve: {
            fallback: {
                stream: "rollup-plugin-node-polyfills/polyfills/stream",
                events: "rollup-plugin-node-polyfills/polyfills/events",
                assert: "assert",
                crypto: "crypto-browserify",
                util: "util",
                fs: false,
                process: false,
                path: false,
                zlib: false,
            }
        }
    }
})