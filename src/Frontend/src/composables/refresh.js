export const autoRefresh = (store) => {
    setInterval(async () => {
        refresh(store);
    }, 15000)
}

export const refresh = async (store) => {
    console.log("refresh");
    await store.loadMyPuzzles();
}