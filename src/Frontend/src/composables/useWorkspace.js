import { computed } from 'vue'
import { useAnchorWallet } from 'solana-wallets-vue'
import { Connection } from '@solana/web3.js'
import { Provider } from '@project-serum/anchor'
import { Metaplex, walletAdapterIdentity } from "@metaplex-foundation/js-next";

const opts = {
    preflightCommitment: "processed"
}

export const initWorkspace = (store) => {
    const wallet = useAnchorWallet()
    const connection = new Connection('https://api.devnet.solana.com', opts.preflightCommitment)
    const provider = computed(() => new Provider(connection, wallet.value, opts.preflightCommitment))
    const metaplex = new Metaplex(connection);
    metaplex.use(walletAdapterIdentity(wallet));

    store.setupWorkspace(
        wallet,
        connection,
        provider,
        metaplex
    )
}
