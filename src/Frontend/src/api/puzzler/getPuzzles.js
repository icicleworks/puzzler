import axios from "axios";
import { PUZZLER_BACKEND } from "@/api/puzzler"

export const getPuzzles = async () => {
    return (await axios.get(PUZZLER_BACKEND + "api/Puzzle")).data
}