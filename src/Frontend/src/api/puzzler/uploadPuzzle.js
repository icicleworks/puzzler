import axios from "axios";
import { PUZZLER_BACKEND } from "@/api/puzzler"

export const uploadPuzzle = async (image, puzzleName) => {
    var formData = new FormData();
    formData.append("image", image);
    formData.append("name", puzzleName);
    axios.post(PUZZLER_BACKEND + 'api/puzzle', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
}