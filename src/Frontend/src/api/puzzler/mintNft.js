import { PUZZLER_BACKEND } from "./constants"

export const mintTileNft = async ({ metaplex }, puzzleName) => {
    let index = Math.floor(Math.random() * 9);
    const { nft } = await metaplex.nfts().create({
        uri: PUZZLER_BACKEND + "api/metadata/" + puzzleName + "/" + index,
        maxSupply: 0, // Unique
        isMutable: false
    });
    return nft;
}

export const mintPuzzleNft = async ({ metaplex }, puzzleName) => {
    const { nft } = await metaplex.nfts().create({
        uri: PUZZLER_BACKEND + "api/metadata/" + puzzleName,
        maxSupply: 0, // Unique
        isMutable: false
    });
    return nft;
}