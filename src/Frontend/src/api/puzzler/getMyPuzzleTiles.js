import axios from "axios";

export const getMyPuzzleTiles = async ({ metaplex }) => {
  let allNfts = await metaplex.nfts().findAllByOwner(metaplex.identity().publicKey);

  let nftMetadatas = [];
  await asyncForEach(allNfts, async (nft) => {
    try {
      let metadata = (await axios.get(nft.uri)).data
      nftMetadatas.push(metadata);
    } catch (err) {
      console.log("");
    }
  });

  return nftMetadatas;
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}