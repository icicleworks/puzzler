import { web3 } from "@project-serum/anchor"

const opts = {
    preflightCommitment: "confirmed",
    commitment: "confirmed"
}

export const getAccountTransactions = async ({ connection }, account) => {
    let signatures = await connection.getSignaturesForAddress(new web3.PublicKey(account), null, opts.preflightCommitment);

    let transactions = []
    for await (let sig of signatures) {
        let trx = await connection.getTransaction(sig.signature, opts);
        transactions.push(trx);
    }
    return transactions;
}