import { web3 } from '@project-serum/anchor'

export const getTokenBalance = async ({ connection }, tokenAddress) => {
    return connection.getTokenAccountBalance(new web3.PublicKey(tokenAddress));
}