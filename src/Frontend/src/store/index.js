import { defineStore } from 'pinia';
import { getMyPuzzleTiles } from "@/api/puzzler";


export const usePuzzlerStore = defineStore('puzzler', {
  state: () => ({
    myPuzzles: [],
    workspace: {}
  }),
  getters: {
    isConnected(state) {
      return state.workspace.wallet != null;
    }
  },
  actions: {
    setupWorkspace(wallet, connection, provider, metaplex) {
      this.workspace = {
        wallet: wallet,
        connection: connection,
        provider: provider,
        metaplex: metaplex
      }
    },
    async loadMyPuzzles() {
      if (this.workspace.wallet) {
        let puzzles = await getMyPuzzleTiles(this.workspace)
        if (this.myPuzzles.length != puzzles.length) {
          this.myPuzzles = puzzles
        }
      } else {
        this.myPuzzles = []
      }
    }
  },
});
