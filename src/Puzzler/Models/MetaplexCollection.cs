﻿using System.Text.Json.Serialization;

namespace Puzzler.Models
{
    public class MetaplexCollection
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("family")]
        public string Family { get; set; }
    }
}
