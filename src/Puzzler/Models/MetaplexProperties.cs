﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Puzzler.Models
{
    public class MetaplexProperties
    {
        [JsonPropertyName("files")]
        public List<MetaplexFile> Files { get; set; }

        [JsonPropertyName("category")]
        public string Category { get; set; }
        [JsonPropertyName("creators")]
        public List<MetaplexCreator> Creators { get; set; }
    }

}
