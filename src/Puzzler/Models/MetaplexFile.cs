﻿using System.Text.Json.Serialization;

namespace Puzzler.Models
{
    public class MetaplexFile
    {
        [JsonPropertyName("uri")]
        public string Uri { get; set; }
        [JsonPropertyName("type")]
        public string Type { get; set; }
    }

}
