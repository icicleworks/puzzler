﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Puzzler.Models
{
    public class MetaplexMetadata
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("symbol")]
        public string Symbol { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("seller_fee_basis_points")]
        public int SellerFeeBasisPoints { get; set; }

        [JsonPropertyName("image")]
        public string Image { get; set; }

        [JsonPropertyName("animation_url")]
        public string AnimationUrl { get; set; }

        [JsonPropertyName("external_url")]
        public string ExternalUrl { get; set; }

        [JsonPropertyName("attributes")]
        public List<MetaplexAttribute> Attributes { get; set; } = new();

        [JsonPropertyName("collection")]
        public MetaplexCollection Collection { get; set; }

        [JsonPropertyName("properties")]
        public MetaplexProperties Properties { get; set; }

    }

}
