﻿using System.Text.Json.Serialization;

namespace Puzzler.Models
{
    public class MetaplexCreator
    {
        [JsonPropertyName("address")]
        public string Address { get; set; }
        [JsonPropertyName("share")]
        public int Share { get; set; }
    }

}
