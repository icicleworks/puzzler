﻿

namespace Puzzler.Services
{
    public interface IPuzzleService
    {
        Task<string> SavePuzzleImage(string name, IFormFile image);
        Task CreateTiles(string path);
        Task<FileStream> GetPuzzleImage(string name);
        Task<FileStream> GetTile(string name, int index);
        Task<IEnumerable<string>> GetAllPuzzles();
    }
}
