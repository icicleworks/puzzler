﻿using System.Drawing;
using System.Drawing.Imaging;

namespace Puzzler.Services.Impl
{
    public class PuzzleService : IPuzzleService
    {
        private readonly string UPLOAD_DIRECTORY_PATH = "Puzzle";
        private readonly IWebHostEnvironment _env;

        public PuzzleService(IWebHostEnvironment env)
        {
            this._env = env;

            UPLOAD_DIRECTORY_PATH = Path.Combine(_env.ContentRootPath, UPLOAD_DIRECTORY_PATH);

            if (!Directory.Exists(UPLOAD_DIRECTORY_PATH))
            {
                Directory.CreateDirectory(UPLOAD_DIRECTORY_PATH);
            }
        }

        public Task CreateTiles(string name)
        {
            var imgarray = new Image[9];
            var img = Image.FromFile(Path.Combine(UPLOAD_DIRECTORY_PATH, name, "puzzle"));
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var index = i * 3 + j;

                    var width = img.Width / 3;
                    var height = img.Height / 3;

                    imgarray[index] = new Bitmap(width, height);
                    var graphics = Graphics.FromImage(imgarray[index]);
                    graphics.DrawImage(img, new Rectangle(0, 0, width, height), new Rectangle(i * width, j * height, width, height), GraphicsUnit.Pixel);

                    imgarray[index].Save(Path.Combine(UPLOAD_DIRECTORY_PATH, name, $"{index}.png"), ImageFormat.Png);

                    graphics.Dispose();
                }
            }

            return Task.CompletedTask;
        }

        public Task<IEnumerable<string>> GetAllPuzzles()
        {
            IEnumerable<string> puzzles = Directory.GetDirectories(UPLOAD_DIRECTORY_PATH).ToList();
            return Task.FromResult(puzzles.Select(p => p.Split("\\").Last()));
        }

        public Task<FileStream> GetPuzzleImage(string name)
        {
            return Task.FromResult(File.OpenRead(Path.Combine(UPLOAD_DIRECTORY_PATH, name, "puzzle")));
        }

        public Task<FileStream> GetTile(string name, int index)
        {
            if (index > 8 || index < 0)
            {
                throw new IndexOutOfRangeException();
            }
            return Task.FromResult(File.OpenRead(Path.Combine(UPLOAD_DIRECTORY_PATH, name, $"{index}.png")));
        }

        public async Task<string> SavePuzzleImage(string name, IFormFile image)
        {
            if (Directory.Exists(Path.Combine(UPLOAD_DIRECTORY_PATH, name)))
            {
                throw new Exception($"Puzzle with name {name} already exists!");
            }

            Directory.CreateDirectory(Path.Combine(UPLOAD_DIRECTORY_PATH, name));

            var imageFormat = "." + image.FileName.Split('.').Last();
            var filePath = Path.Combine(UPLOAD_DIRECTORY_PATH, name, "puzzle");

            using var stream = File.Create(filePath);
            await image.CopyToAsync(stream);
            await stream.DisposeAsync();

            return filePath;
        }
    }

}
