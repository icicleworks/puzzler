﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Puzzler.Models;

namespace Puzzler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetadataController : ControllerBase
    {
        [HttpGet("{puzzleName}/{tile}")]
        public async Task<IActionResult> GetTileMetadata(string puzzleName, int tile)
        {
            var metadata = new MetaplexMetadata()
            {
                Name = puzzleName,
                Symbol = "PZZLR",
                Collection = new MetaplexCollection() { Name = puzzleName, Family = "Puzzler"},
                Description = "Make puzzle NFTs",
                Image = $"{Request.Scheme}://{Request.Host}/api/puzzle/{puzzleName}/{tile}"
            };

            metadata.Attributes.Add(
                new MetaplexAttribute()
                { 
                    TraitType = "position",
                    Value = tile.ToString() 
                });

            return Ok(metadata);
        }

        [HttpGet("{puzzleName}")]
        public async Task<IActionResult> GetPuzzleMetadata(string puzzleName)
        {
            var metadata = new MetaplexMetadata()
            {
                Name = puzzleName,
                Symbol = "PZZLR",
                Collection = new MetaplexCollection() { Name = puzzleName, Family = "Puzzler" },
                Description = "Make puzzle NFTs",
                Image = $"{Request.Scheme}://{Request.Host}/api/puzzle/{puzzleName}"
            };

            metadata.Attributes.Add(
                new MetaplexAttribute()
                {
                    TraitType = "Super power",
                    Value = "Vie plávať"
                });

            return Ok(metadata);
        }
    }
}
