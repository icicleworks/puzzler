﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Puzzler.Services;

namespace Puzzler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuzzleController : ControllerBase
    {

        private readonly IPuzzleService _puzzleService;

        public PuzzleController(IPuzzleService puzzleService)
        {
            _puzzleService = puzzleService;
        }

        [HttpPost]
        public async Task<IActionResult> Upload([FromForm] string name, [FromForm] IFormFile image)
        {
            if (image is null)
            {
                return BadRequest("Upload image");
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                return BadRequest("Set puzzle name name");
            }

            await _puzzleService.SavePuzzleImage(name, image);
            await _puzzleService.CreateTiles(name);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var puzzles = await _puzzleService.GetAllPuzzles();
            return Ok(puzzles);
        }

        [HttpGet("{puzzle}")]
        public async Task<IActionResult> Get(string puzzle)
        {
            var image = await _puzzleService.GetPuzzleImage(puzzle);
            return File(image, "image/png", $"{puzzle}.png");
        }

        [HttpGet("{puzzle}/{tile:int}")]
        public async Task<IActionResult> GetTile(string puzzle, int tile)
        {
            var image = await _puzzleService.GetTile(puzzle, tile);
            return File(image, "image/png", $"{tile}.png");
        }

        [HttpGet("{puzzle}/random")]
        public async Task<IActionResult> GetRandomTile(string puzzle)
        {
            var tile = new Random().Next(9);
            var image = await _puzzleService.GetTile(puzzle, tile);
            return File(image, "image/png", $"{tile}.png");
        }
    }
}
