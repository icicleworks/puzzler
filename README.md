# Puzzler

Solana NFT puzzle "game". 
1. Uživateľ si na hlavnej stránke pripojí Solana penaženku a vyberie alebo nahrá vlastný obrázok
2. Pod obrázkom je možné mintovať časti obrázku ako jednotlivé puzzle
3. Po získaní všetkých kusov obrázku je možné mintovať celý obrázok ako NFT

## src/Puzzler

Asp.net backend ktorý umožní upload obrázku a rozdelí ho na 9 kusov ako puzzle.    
Backend slúží zároveň aj ako úložisko pre nft obrázky `Controllers/PuzzleController` a metadata `Controllers/MetadataController`.    
V priečinku `src/Puzzler/publish` je pripravený build pre windows. Banckend sa spustí na portoch 5001(https) a 5000(http)

## src/Frontend

Vue.js frontend umožnuje pripojenie Solana wallet a mintovanie Puzzle ako NFT.    
Na NFT metadata je použitý smart contract metaplex a samotné NFT je vytvorené ako Master Edition 1/1    
Frontend je možné spustiť cez `cd src/Frontend && yarn serve` na porte 8080 
